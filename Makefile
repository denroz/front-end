build-develop:
	sudo docker-compose -f docker-compose.dev.yml build
	sudo docker-compose -f docker-compose.dev.yml push

build-prod:
	sudo docker-compose -f docker-compose.prod.yml build
	sudo docker-compose -f docker-compose.prod.yml push

deploy-develop:
	git pull origin develop
	make build-develop
	sudo docker stack rm frontend
	sudo docker stack deploy -c docker-compose.dev.yml frontend

deploy-prod:
	git pull origin master
	make build-prod
	sudo docker stack rm frontend
	sudo docker stack deploy -c docker-compose.prod.yml frontend

up-local:
	sudo docker-compose -f docker-compose.local.yml up