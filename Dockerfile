FROM node:14.17-alpine as build

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY . ./

RUN yarn install && \
    yarn build

# production environment
FROM nginx:stable-alpine

ARG APP_PAGINATION_NUM=10
ARG APP_API=/api/v1/
ARG APP_SERVER=https://api.liketo.me

ENV REACT_APP_PAGINATION_NUM=${APP_PAGINATION_NUM}
ENV REACT_APP_API=${APP_API}
ENV REACT_APP_SERVER=${APP_SERVER}

COPY conf/nginx.conf /etc/nginx/conf.d/default.conf

COPY --from=build /app/build /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]