import MainContent from './MainContent';
import PageInner from './PageInner';
import PageContent from './PageContent';
import LoginPagesContainer from './LoginPagesContainer';

export { MainContent, PageInner, PageContent, LoginPagesContainer };
