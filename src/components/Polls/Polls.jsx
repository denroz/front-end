import React, { useState, useEffect, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { Map } from 'react-lodash';
import GlobalContext from '../../context/globalContext';
import ServiceAPI from '../../services';
import PageContent from '../StyleComponents/PageContent';
import AddButton from '../Buttons/AddButton';
import PollRow from './PollRow';
import Pagination from '../Pagination/Pagination';
import usePagination from '../../hooks/usePagination';
import { ReactComponent as IconSort } from '../../img/sprite/svg/sort.svg';
import { ReactComponent as IconPlus } from '../../img/sprite/svg/plus.svg';
import Loading from '../Loading/Loading';
import { loading } from '../../actions';

const Polls = () => {
  const { t } = useTranslation();
  const service = new ServiceAPI();
  const { dispatchGlobal, stateGlobal } = useContext(GlobalContext);
  const userIsOwnerOrAdmin =
    stateGlobal.user?.is_owner || stateGlobal.user?.is_admin;

  const [polls, setPolls] = useState([]);
  const [count, setCount] = useState(0);
  const [searchText, setSearchText] = useState(null);

  const { next, prev, jump, currentPage, setCurrentPage, maxPage } =
    usePagination(count, process.env.REACT_APP_PAGINATION_NUM);

  useEffect(() => {
    dispatchGlobal(loading(true));
    service.getPolls().then((resp) => {
      if (resp.status === 200) {
        setCount(resp.data.count);
        setPolls(resp.data.results);
        dispatchGlobal(loading(false));
      }
    });
  }, []);

  useEffect(() => {
    // Search
    if (searchText !== null) {
      const timeout = setTimeout(() => {
        dispatchGlobal(loading(true));
        const searchObj = searchText && { search: searchText };
        service.getPolls(searchObj).then((resp) => {
          if (resp.status === 200) {
            setCount(resp.data.count);
            setPolls(resp.data.results);
            setCurrentPage(1);
            dispatchGlobal(loading(false));
          }
        });
      }, 1000);

      // if this effect run again, because `inputValue` changed, we remove the previous timeout
      return () => clearTimeout(timeout);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchText]);

  const changePage = (pageNum) => {
    if (currentPage !== pageNum) {
      dispatchGlobal(loading(true));
      const paginationObj = {
        limit: process.env.REACT_APP_PAGINATION_NUM,
        offset:
          pageNum * process.env.REACT_APP_PAGINATION_NUM -
          process.env.REACT_APP_PAGINATION_NUM
      };
      if (searchText) paginationObj.search = searchText;
      service.getPolls(paginationObj).then((resp) => {
        if (resp.status === 200) {
          setPolls(resp.data.results);
          dispatchGlobal(loading(false));
        }
      });
    }
  };

  return (
    <PageContent>
      <div className="content__header">
        <h1 className="content__title">{t('pollsPage.pageName')}</h1>
        {/* <AddButton
          text={t('pollsPage.createNewPoll')}
          linkTo="/polls/pollCreate"
        /> */}
      </div>
      <div className="content__search">
        <div className="page-search">
          <form>
            <div className="form-group">
              <input
                className="form-control"
                type="text"
                placeholder={t('pollsPage.search')}
                value={searchText || ''}
                onChange={(e) => setSearchText(e.target.value)}
              />
            </div>
          </form>
        </div>
      </div>
      <div className="content__panels">
        <div className="polls">
          <div className="polls__panel">
            <div className="polls-panel">
              <div className="polls-panel__head">
                <div className="col-4 sortable-link">
                  {/* <a className="sortable-link"> */}
                  {t('pollsPage.name')}
                  {/* <IconSort className="svg-sprite-icon icon-sort" /> */}
                  {/* </a> */}
                </div>
                <div className="col-4 sortable-link">
                  {/* <a className="sortable-link"> */}
                  {t('pollsPage.question')}
                  {/* <IconSort className="svg-sprite-icon icon-sort" /> */}
                  {/* </a> */}
                </div>
                <div className="col-1 text-center sortable-link sortable-link__link-title">
                  {/* <a className="sortable-link"> */}
                  {t('pollsPage.link')}
                  {/* <IconSort className="svg-sprite-icon icon-sort" /> */}
                  {/* </a> */}
                </div>
                <div className="col-2 text-center sortable-link">
                  {/* <a className="sortable-link"> */}
                  {t('pollsPage.status')}
                  {/* <IconSort className="svg-sprite-icon icon-sort" /> */}
                  {/* </a> */}
                </div>
                <div className="col-1" />
              </div>
              {stateGlobal.isLoading ? (
                <Loading />
              ) : (
                <Map
                  collection={polls}
                  iteratee={(item) => {
                    if (!item.is_total) {
                      return (
                        <PollRow
                          key={item?.id}
                          id={item?.id}
                          name={item?.title}
                          question={item?.questions[0]?.body}
                          status={item?.is_active}
                          userIsOwnerOrAdmin={userIsOwnerOrAdmin}
                          voteLink={item.vote_link}
                        />
                      );
                    }
                  }}
                />
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="content__pagination content__pagination--has-btn">
        {/* <Link className="btn-panel-add" to="/polls/pollCreate">
          <span>
            {t('pollsPage.createNewPoll')}
            <span className="icon-btn">
              <IconPlus className="svg-sprite-icon icon-plus" />
            </span>
          </span>
        </Link> */}
        <Pagination
          currentPage={currentPage}
          maxPage={maxPage}
          next={next}
          prev={prev}
          jump={jump}
          changePage={changePage}
        />
      </div>
    </PageContent>
  );
};

export default Polls;
