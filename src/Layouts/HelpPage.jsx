import React from 'react';
import { useTranslation } from 'react-i18next';
import PageContent from '../components/StyleComponents/PageContent';
import DefaultInner from './DefaultInner';

const HelpPage = () => {
  const { t } = useTranslation();

  return (
    <DefaultInner>
      <PageContent>
        <div className="content__header">
          <h1 className="content__title">{t('UserMenuPopUpWindow.faq')}</h1>
        </div>
        <div className="statistics-start" />
      </PageContent>
    </DefaultInner>
  );
};

export default HelpPage;
