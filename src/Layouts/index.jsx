import AccountPage from './AccountPage';
import UsersPage from './UsersPage';
import DevicesPage from './DevicesPage';
import LoginPage from './LoginPage';
import PollsPage from './PollsPage';
import RatingPage from './RatingPage';
import ReportsPage from './ReportsPage';
import ResetPasswordPage from './ResetPasswordPage';
import StatisticPage from './StatisticPage';
import NotFoundPage from './NotFoundPage';
import ServerErrorPage from './ServerErrorPage';
import QrVotePage from './QrVotePage';
import HelpPage from './HelpPage';
import ReviewsPage from './ReviewsPage';
import BackStagePage from './BackStagePage';

export {
  AccountPage,
  UsersPage,
  DevicesPage,
  LoginPage,
  PollsPage,
  RatingPage,
  ReportsPage,
  ResetPasswordPage,
  StatisticPage,
  NotFoundPage,
  ServerErrorPage,
  QrVotePage,
  HelpPage,
  ReviewsPage,
  BackStagePage
};
