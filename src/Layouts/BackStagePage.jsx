import React from 'react';
import { Link } from 'react-router-dom';

const links = [
  {
    to: 'e6aa82b4-e5fe-4160-9b1a-210b00e02329',
    label: 'BACKSTAGE MISHUHY'
  },
  {
    to: '97b07e1b-65f7-4e8c-b92a-f7a07403cc2f',
    label: 'BACKSTАGE OBOLON'
  },
  {
    to: 'e8ebe949-38f9-4fa5-84c1-e5611f668d64',
    label: 'BACKSTАGE VELYKA VASYLKIVSKA'
  },
  {
    to: '4dedebf7-aefa-44d7-a19d-a698bc66b376',
    label: 'BACKSTАGE LOBANOVSKOHO'
  },
  {
    to: '8e18a416-74cd-4964-a7aa-33fb407095ed',
    label: 'BACKSTАGE CHORNOVOLA'
  },
  {
    to: '50737d7b-6e48-46ef-aa7d-c5d4bafd9901',
    label: 'BACKSTАGE OSOKORKY'
  },
  {
    to: '6cac373f-c4d1-4862-af63-b30f93ff9ddc',
    label: 'BACKSTАGE COMFORT TOWN'
  },
  {
    to: 'acad54e0-db0c-480e-b004-f3b6cb327658',
    label: 'BACKSTАGE KLOVSKYI'
  },
  {
    to: '936fd91f-a664-4bff-91bd-fc038d834d27',
    label: 'BACKSTАGE LYPKY'
  },
  {
    to: '5a474463-c93f-4eb4-8ad8-a3e47a9d5f5a',
    label: 'BACKSTАGE PECHERSK'
  },
  {
    to: '34daf3c1-1bd8-4e99-99e7-1dbfd4bd6495',
    label: 'BACKSTАGEHOTEL HILTON KYIV'
  },
  {
    to: '98768da9-0efd-484e-a7cc-a160bb29d204',
    label: 'BACKSTАGE TEREMKY'
  },
  {
    to: '398a4e67-057a-4770-9372-2693f7dd4a08',
    label: 'BACKSTАGE GRAND PRIX'
  }
];

const BackStagePage = () => {
  return (
    <div className="page__inner">
      <div className="backstage">
        <div className="backstage__container">
          <div className="backstage__header">B A C K S T A G E</div>
          <div className="backstage__text">
            <p>Ми хочемо ставати краще для вас!</p>
            <p>Будемо вдячні за зворотний зв'язок</p>
            <p>Виберіть локацію обслуговування 👇</p>
          </div>
          <div className="backstage__buttons-containers">
            {links.map((item, index) => {
              return (
                <Link key={index} to={`/device/vote/${item.to}`}>
                  <button className="backstage__button">{item.label}</button>
                </Link>
              );
            })}
          </div>
          <div className="page-qr__footer">
            <p style={{ color: '#42423e' }}>
              Розроблено в &nbsp;
              <a href="https://liketo.me/" target="_blank" rel="noreferrer">
                liketo.me
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BackStagePage;
